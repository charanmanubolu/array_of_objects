//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.
function hobbiesOfSpecificAgedPerson(arr, age) {
    const result = []
    if (Array.isArray(arr) && typeof (age) === 'number') {
        for (let element of arr) {
            if (element.age === age) {
                result.push(element.hobbies)
            }
        }
    }
    return result
}


module.exports = hobbiesOfSpecificAgedPerson;