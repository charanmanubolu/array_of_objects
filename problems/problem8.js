
//    Implement a loop to access and log the city and country of each individual in the dataset.

function cityAndCounty(array) {
  const result = []
  if (Array.isArray(array)) {
    for (let element of array) {
      result.push(element.city, element.country)
    }
  } else {
    console.error([]);
  }
  return result
}
module.exports = cityAndCounty