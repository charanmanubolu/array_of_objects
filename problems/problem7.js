
//    Write a function that accesses and prints the names and email addresses of individuals aged 25.
function namesAndMailsOfage25(arr, age) {
  const result = []
  if (Array.isArray(arr) && typeof(age) =='number') {
    for (let element of arr ) {
      if (element.age === age) {
        result.push(element.name, element.email)
      }
    }
  } else {
    console.error([]);
  }
  return result
}
module.exports = namesAndMailsOfage25