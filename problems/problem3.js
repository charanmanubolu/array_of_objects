//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.
function studentsLiveInAustalia(arr) {
    let result =[]
    if(Array.isArray(arr)){
    for(let element of arr){
        if(element.isStudent&& element.country === 'Australia'){
            result.push(element.name)
        }
    }
}else{
    console.error()
}
    return result
    
  }
  module.exports=studentsLiveInAustalia